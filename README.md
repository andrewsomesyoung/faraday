# Faraday

*A proof-of-concept project to inform our choice of tools and technologies for SilverPlatform.*

## Overview

There are a number of off-the-shelf tools and technologies available that we could leverage for the SilverPlatform effort. Each has pros and cons and once we decide to go with one, we'll be living with that decision for a while.

The goal of Project Faraday is to gain hands-on experience with several of the tools out there, understand how we could solve several of the challenges we know we'll face, compare them, and make an informed choice.

Without investing months in research...

## Goals

* **One Application / Multiple Toolchains:** Deploy the same sample application on multiple container orchestration enginese.g. Nomad, Kubernetes, DCOS, Swarm, etc

* **User Stories:** For each toolchain, implement a common set of "user stories" designed to exercise the core features we know we'll neede.g. self-serve environments, deployments, dynamic configuration, auto scaling, etc

* **Share the Love:** We're picking a toolset for multiple groups of people, in theory the whole organization. Getting ones hands dirty with these tools is really important. It's not sufficient for one or two people to do a deep dive and then make a recommendation. A key output of this POC will be an easy way for others to reproduce the results. Ideally, each toolchain will have a repo somewhere with a README.md that provides an overview and all the necessary sources for anyone to pick it up and try it out.

## The Application

The sample application has 3 components: Alpha, Beta, and Gamma. Alpha and Beta are both SpringBoot-based web services. Gamma is a Lambda function exposed as a web service via Amazon's API Gateway.

All three implement a simple GET request. Gamma returns a fixed string. Alpha and Beta concatenate a configurable **name** setting with the response from the next component in line. Given default values of Alpha and Beta, the client should see "Alpha Beta Gamma".

![Components](img/components.png)

### Acceptance Tests

Once deployed, all of the jasmine-node tests in the tests folder should pass. For example,

~~~javascript
// Imports
var request = require('request');

// Globals
var url = process.env.ALPHA_URL || "http://localhost:8080/alpha";

describe("Alpha", function() {
    it("returns default answer", function(done) {
        
        // Send GET request
        request.get(url, function(error, response, body) {
            if (error || response.statusCode != 200) {
                return done(error || "Unexpected response: " + response.statusCode);
            }
            
            // Verify answer
            expect(body).toEqual("Alpha Beta Gamma");
            
            done();
        });
        
    });
});
~~~

![Jasmine Test Results](img/jasmine-node.png)

### Dependencies

Building and running the basics here requires the following. As we dig into each of the toolchains, there will no doubt be more added.

* Java 8
* Maven
* Docker
* Packer

### Building

The Faraday source tree is organized into multiple Maven modules all accessible from the top level. Packer is integrated into the Maven build during the package phase.

*Docker for Mac Note:* I found I had to use the aufs storage driver (add "storage-driver" : "aufs" to daemon.json) or the MySQL containers wouldn't build.

#### Build Everything

To go from source to deployable containers, you can simply direct your terminal at the top folder and run:

~~~
mvn package
~~~

The initial build may take a few minutes because it builds all of the base containers from scratch. Once built, only the modified components will be rebuilt.

#### Forcing a Rebuild

Running mvn package from the top will rebuild anything that changed. This is usually sufficient. However, you can force a rebuild by running:

~~~
mvn clean package
~~~

### Containers

The build currently generates the following container hierarchy in your local Docker:

![Containers](img/containers.png)

### Configuration

Both Alpha and Beta are configured using properties and should work with any of SpringBoot's external configuration techniques. Specifically, the settings are

#### Alpha

* alpha.name
* beta.url

#### Beta

* beta.name
* gamma.url

### Testing in Local Docker

Build Everything:

~~~
mvn package
~~~

Login to AWS and deploy Gamma in Lambda:

~~~
aws lambda ...
~~~

Lookup Gamma URL and inject into Beta:

~~~
docker run -P -e "gamma.url=https://d2zo427mi9.execute-api.us-east-2.amazonaws.com/prod/gamma" -d beta
~~~

Lookup Beta IP and then inject into Alpha:

~~~
docker run -P -e "beta.url=http://172.17.0.3:8080/beta" -d alpha
~~~

Lookup Alpha IP and then run tests:

~~~
ALPHA_URL=http://localhost:32774/alpha BETA_URL=http://localhost:32770/beta GAMMA_URL=https://d2zo427mi9.execute-api.us-east-2.amazonaws.com/prod/gamma jasmine-node --verbose tests
~~~

For example, 

![Jasmine Test Results](img/jasmine-node2.png)

## User Stories

By implementing each of these stories on each platform, we can make sure we've thoroughly kicked the tires and thought through some of the more advanced use cases.

### Provision: AWS

I want to create a new empty cluster running in AWS.

### Deploy

Starting from an empty cluster, I want to deploy all components (containers to passing tests) in less than 30 seconds.

### Redeploy: Rolling Upgrades

Starting from a running cluster, I want to upgrade one of the components in place without touching the other ones.

### Redeploy: Blue / Green

Starting from a running cluster, I want to redeploy all components using a blue green approach where all of the components are deployed in isolation and tested before traffic is gradually transitioned from the previous version. 

### Redeploy: Blue / Green & Canary

Starting with a running cluster, I want to simulate moderate traffic and run a blue/green upgrade of one of the components with a bad memory leak that starts to fail after the traffic starts to transition. Once the failures start, I want to rollback to the previous version.

### Monitor Deploy

Starting from an empty cluster, I want to deploy both components and monitor their state of health, answering questions like:

* Is the deploy still in progress or did it succeed or fail?
* How much of my cluster is in use before and after the deploy?
* Where can I access each of the components?

### Monitor Cluster

Starting from a cluster with a single instance of each component and auto scaling disabled, I want to perform a load test that pushes at least one component beyond its capacity. During the test I want to monitor:

* Response latency
* CPU
* Memory

### Auto Scale

Starting from a cluster with a single instance of each component and auto scaling enabled, I want to perform a load test that pushes the cluster beyond its capacity and have the system automatically add more resources to the cluster and reschedule components as necessary to handle the increased load. Reducing the traffic, I want to see the system auto scale down.

### Tune Auto Scaling

As the system scales up and down, I want to visualize the system's behavior over time and adjust the thresholds to control it.

### Monitor Application

Starting with a running cluster, I want to simulate very light traffic and monitor what the application is doing. I should be able to understand the sequence of events as the request propagates through the system from Alpha to Beta to BetaDB.

### Error Handling

Starting with a running cluster, I want to inject an error every Nth request. The system should provide some mechanism for collecting the errors, visualizing them, and diagnosing what is failing and why.

### Alert

Starting from a running cluster, I want to "disable" (specifics TBD) one of the component instances and receive a notification the system detected the event.

### Recover

Starting from an HA cluster, I want to

1. Start a load test to simulate ongoing web service traffic
1. "Disable" (specifics TBD) one of the component instances
1. Have the system automatically recover without failing the load test






 


