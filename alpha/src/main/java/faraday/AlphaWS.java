package faraday;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class AlphaWS
{
    @Value("${alpha.name}")
    private String name;
    
    @Value("${beta.url}")
    private String betaUrl;
    
    @RequestMapping("/alpha")
    public String alpha()
    {
        RestTemplate restTemplate = new RestTemplate();
        
        String betaResponse = restTemplate.getForObject(betaUrl, String.class);
        
        return name + " " + betaResponse;
    }
}
