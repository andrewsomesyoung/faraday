package faraday;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class BetaWS
{
    @Value("${beta.name}")
    private String name;
    
    @Value("${gamma.url}")
    private String gammaUrl;
    
    @RequestMapping("/beta")
    public String beta()
    {
        RestTemplate restTemplate = new RestTemplate();
        
        String gammaResponse = restTemplate.getForObject(gammaUrl, String.class).replaceAll("\"", "");
        
        return name + " " + gammaResponse;
    }
}
