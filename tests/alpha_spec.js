// Imports
var request = require('request');

// Globals
var url = process.env.ALPHA_URL || "http://localhost:8080";

describe("Alpha", function() {
    it("returns default answer", function(done) {
        
        // Send GET /whatsyourname request
        request.get(url + "/whatsyourname", function(error, response, body) {
            if (error || response.statusCode != 200) {
                return done(error || "Unexpected response: " + response.statusCode);
            }
            
            // Verify answer
            expect(body).toEqual("Alpha Beta 1");
            
            done();
        });
        
    });
});

