// Imports
var request = require('request');

// Globals
var url = process.env.BETA_URL || "http://localhost:8080/beta";

describe("Beta", function() {
    it("returns default answer", function(done) {
        
        // Send GET request
        request.get(url, function(error, response, body) {
            if (error || response.statusCode != 200) {
                return done(error || "Unexpected response: " + response.statusCode);
            }
            
            // Verify answer
            expect(body).toEqual("Beta Gamma");
            
            done();
        });
        
    });
});

